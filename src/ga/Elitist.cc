#include "Elitist.h"

#include <algorithm>
#include <iostream>

#include "ga/Chromosom.h"
#include "ga/Population.h"


//////////////////////// Helpers ////////////////////////

namespace {

/**
 * @brief Defines a compare function between two Chromosoms.
 */
bool compareFitness(ga::Chromosom *c1, ga::Chromosom *c2)
{
    return (c1->fitness() < c2->fitness());
}

/**
 * @brief Sorts the vector of chromosoms defined by 'chrs'.
 */
void sortChromosoms(std::vector<ga::Chromosom *> *chrs)
{
    std::sort(chrs->begin(), chrs->end(), compareFitness);
}

} // anon namespace


namespace ga {


//////////////////////// Elitist ////////////////////////

/**
 * @brief Returns the 'pr' best individuals from the current population.
 */
std::vector<Chromosom *> Elitist::select(unsigned int pr) const
{
    // Sort the chromosoms of the given population
    std::vector<Chromosom *> chrs = m_pop->getChromosoms();
    sortChromosoms(&chrs);

    // Return the best chromosoms
    std::vector<Chromosom *>::const_iterator begIt = chrs.begin();

    int bestCount = (m_pop->getCount() * pr) / 100;

    // Round the result if it's < 1
    if (!bestCount)
        bestCount = 1;

    return std::vector<Chromosom *>(begIt, begIt + bestCount);
}

} // namespace ga
