#include "Population.h"

#include <algorithm>
#include <assert.h>
#include <iostream>

namespace ga {

int Population::m_nextName = 0;

Population::Population() :
    m_name(m_nextName),
    m_chromosoms()
{
    m_nextName++;
}

Population::Population(const std::vector<Chromosom *> &chromosoms) :
    m_name(m_nextName),
    m_chromosoms(chromosoms)
{
    m_nextName++;
}

Population::~Population() 
{
    std::vector<Chromosom *>::iterator it = m_chromosoms.begin();

    while (it != m_chromosoms.end()) {
        delete *it;
        it = m_chromosoms.erase(it);
    }
}

int Population::getName() const
{
    return m_name;
}

void Population::addChromosom(Chromosom *chromosom)
{
    if (std::find(m_chromosoms.begin(), m_chromosoms.end(), chromosom) ==
            m_chromosoms.end())
        m_chromosoms.push_back(chromosom);
}

void Population::addChromosoms(const std::vector<Chromosom *> &chrs)
{
    std::vector<Chromosom *>::const_iterator it;

    for (it = chrs.begin(); it != chrs.end(); ++it) {
        addChromosom(*it);   
    }
}

void Population::removeChromosom(Chromosom *chromosom)
{
    // Remove-erase
    std::vector<Chromosom *>::iterator toRm = std::remove(m_chromosoms.begin(),
                                              m_chromosoms.end(),
                                              chromosom);

    m_chromosoms.erase(toRm, m_chromosoms.end());
}

std::vector<Chromosom *> Population::getChromosoms() const 
{
    return m_chromosoms;
}

std::size_t Population::getCount() const
{
    return m_chromosoms.size();
}

bool Population::contains(Chromosom *chr) const
{
    assert(chr);

    std::vector<Chromosom *>::const_iterator end = m_chromosoms.end();

    std::vector<Chromosom *>::const_iterator ret;
    ret = std::find(m_chromosoms.begin(), end, chr);

    return (ret != end);
}

} // namespace ga
