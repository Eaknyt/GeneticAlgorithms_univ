#include "Chromosom.h"

#include <algorithm>
#include <iostream>

#include "ga/Gene.h"

namespace ga {

int Chromosom::m_nextName = 0;

Chromosom::Chromosom() :
	m_name(m_nextName) 
{
	m_nextName++;
}

Chromosom::Chromosom(const std::vector<Gene *> &n) :
	m_name(m_nextName) 
{
	for (std::size_t i = 0; i < n.size(); i++) {
		m_genes.push_back(n[i]);
	}
	
	m_nextName++;
}

Chromosom::Chromosom(const Chromosom &other) :
    m_name(other.m_name)
{}

Chromosom::~Chromosom()
{}

int Chromosom::getName() const
{
	return m_name;
}

std::size_t Chromosom::getLength() const 
{
	return m_genes.size();
}

void Chromosom::insert(Gene *gene) 
{
	m_genes.push_back(gene);
}

void Chromosom::remove(Gene *gene) 
{
	// Remove-erase
    std::vector<Gene *>::iterator toRm = std::remove(m_genes.begin(),
                                                     m_genes.end(),
                                                     gene);

    m_genes.erase(toRm, m_genes.end());

}

std::vector<Gene *> Chromosom::getGenes() const
{
	return m_genes;
}

std::vector<Gene *> &Chromosom::getGenes_ref()
{
    return m_genes;
}

/**
 * @brief Returns true if the Chromosom contains an similar gene of 'gene'.
 *
 */
bool Chromosom::containsGene(Gene *gene) const
{
    std::vector<Gene *>::const_iterator it = m_genes.begin();

    while (it != m_genes.end()) {
        Gene *g = (*it);

        if (*g == *gene)
            return true;

        ++it;
    }

    return false;
}

std::ostream &operator<<(std::ostream &os, const Chromosom &chr)
{
    chr.print(os);

    return os;
}

} // namespace ga
