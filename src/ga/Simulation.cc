#include "ga/Simulation.h"

#include <assert.h>
#include <iostream>

#include "ga/ChrFactory.h"
#include "ga/Crossover.h"
#include "ga/Elitist.h"
#include "ga/Mutator.h"
#include "ga/Population.h"

#include "utils/utilsfuncs.h"

namespace ga {

Simulation::Simulation() :
    m_initialPop(0),
    m_factory(0),
    m_iterationCount(0),
    m_individualCount(0),
    m_blockElementCount(0),
    m_elitismPr(0),
    m_crossoverTech(Crossover::OnePointCrossOrdered),
    m_crossoverPr(0),
    m_mutationPr(0)
{}

Simulation::~Simulation()
{
    delete m_factory;
}

Chromosom *Simulation::start()
{
    unsigned int geneCount = blockElementCount();
    unsigned int chrCount = individualCount();

    std::vector<ga::Chromosom *> initialChrs = m_factory->createVariations
                                               (
                                                    geneCount, chrCount
                                               );

    m_initialPop = new ga::Population(initialChrs);
    updateOperators();

    return initialChrs[0];
}

void Simulation::next()
{
    assert(m_initialPop);
    assert(m_factory);

    checkParams();

    std::vector<Chromosom *> currentChrs = m_initialPop->getChromosoms();

    // Apply elitism
    Elitist *elitism = Elitist::instance();

    std::vector<Chromosom *> bestIndividuals = elitism->select(m_elitismPr);

    // Retrieve the remaining individuals of the current population
    std::vector<Chromosom *> remaining;
    std::vector<Chromosom *>::const_iterator it = currentChrs.begin();

    while (it != currentChrs.end()) {
        Chromosom *chr = (*it);

        if (std::find(bestIndividuals.begin(), bestIndividuals.end(), chr)
            == bestIndividuals.end()) {
            remaining.push_back(chr);
        }

        ++it;
    }

    // Apply crossover
    Crossover *crossover = Crossover::instance();

    std::vector<Chromosom *> toCross = utils::randomPart
                                        (
                                            remaining, m_crossoverPr
                                        );

    it = toCross.begin() + 1;
    std::vector<Chromosom *> children;

    while (it != toCross.end()) {
        // Retrieve two parents
        Chromosom *father = *(it - 1);
        Chromosom *mother = (*it);

        // -18
        Chromosom *s1 = m_factory->create();
        Chromosom *s2 = m_factory->create();

        crossover->cross(father, mother, s1, s2, m_crossoverTech);

        // Retrieve the children
        children.push_back(s1);
        children.push_back(s2);

        ++it;
    }

    // Apply mutation
    Mutator *mutator = Mutator::instance();
    std::vector<Chromosom *> mutated = mutator->mutate(remaining,
                                                       m_mutationPr);

    // Create a new population
    Population *newPop = new Population;
    newPop->addChromosoms(bestIndividuals);
    newPop->addChromosoms(children);
    newPop->addChromosoms(mutated);

    // Retrieve some individuals from the previous population to keep a
    //  constant population size
    std::size_t toReinjectCount = currentChrs.size() - 
                                  (
                                    bestIndividuals.size() + children.size() +
                                    mutated.size()
                                  );

    std::size_t i = 0;
    it = currentChrs.begin();

    while (i < toReinjectCount && it != currentChrs.end()) {
        Chromosom *chr = (*it);

        if (!newPop->contains(chr)) {
            newPop->addChromosom(chr);
            ++i;
        }

        ++it;
    }

    cleanupPreviousPopulation(newPop);

    delete m_initialPop;

    m_initialPop = newPop;
    updateOperators();
}

/**
 *
 */
Chromosom *Simulation::currentBest() const
{
    return Elitist::instance()->select(0)[0];
}

/**
 *
 */
unsigned int Simulation::iterationCount() const
{
    return m_iterationCount;
}

/**
 *
 */
void Simulation::setIterationCount(unsigned int count)
{
    if (m_iterationCount != count)
        m_iterationCount = count;
}

/**
 *
 */
unsigned int Simulation::individualCount() const
{
    return m_individualCount;
}

/**
 *
 */
void Simulation::setIndividualCount(unsigned int count)
{
    if (m_individualCount != count)
        m_individualCount = count;
}

/**
 *
 */
unsigned int Simulation::blockElementCount() const
{
    return m_blockElementCount;
}

/**
 *
 */
void Simulation::setBlockElementCount(unsigned int count)
{
    if (m_blockElementCount != count)
        m_blockElementCount = count;
}

/**
 *
 */
unsigned int Simulation::elitismPr() const
{
    return m_elitismPr;
}

/**
 *
 */
void Simulation::setElitismPr(unsigned int pr)
{
    if (m_elitismPr != pr)
        m_elitismPr = pr;
}

/**
 *
 */
Crossover::CrossTechnique Simulation::crossoverTechnique() const
{
    return m_crossoverTech;
}

/**
 *
 */
void Simulation::setCrossoverTechnique(Crossover::CrossTechnique tech)
{
    if (m_crossoverTech != tech)
        m_crossoverTech = tech;
}

/**
 *
 */
unsigned int Simulation::crossoverPr() const
{
    return m_crossoverPr;
}

/**
 *
 */
void Simulation::setCrossoverPr(unsigned int pr)
{
    if (m_crossoverPr != pr)
        m_crossoverPr = pr;
}

/**
 *
 */
unsigned int Simulation::mutationPr() const
{
    return m_mutationPr;
}

/**
 *
 */
void Simulation::setMutationPr(unsigned int pr)
{
    if (m_mutationPr != pr)
        m_mutationPr = pr;
}

void Simulation::checkParams()
{
    assert(m_iterationCount);
    assert(m_individualCount);
    assert(m_blockElementCount);
    assert(m_elitismPr);
    assert(m_crossoverPr);
    assert(m_mutationPr);
}

void Simulation::updateOperators()
{
    Elitist::instance()->assign(m_initialPop);
    Crossover::instance()->assign(m_initialPop);
    Mutator::instance()->assign(m_initialPop);
}

void Simulation::cleanupPreviousPopulation(Population *newPop)
{
    std::vector<Chromosom *> oldChrs = m_initialPop->getChromosoms();

    std::vector<Chromosom *>::const_iterator it = oldChrs.begin();

    while (it != oldChrs.end()) {
        Chromosom *chr = (*it);
        
        if (newPop->contains(chr))
            m_initialPop->removeChromosom(chr);

        ++it;
    }
}

} // namespace ga
