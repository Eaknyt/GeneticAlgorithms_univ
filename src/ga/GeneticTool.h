#ifndef H_GENETICTOOL
#define H_GENETICTOOL

#include "utils/Singleton.h"
#include "utils/utilsfuncs.h"

namespace ga {

class Population;

template <class T>
class GeneticTool : public utils::Singleton<T>
{
    friend class utils::Singleton<T>;

public:
    void assign(Population *pop)
    {
        if (m_pop != pop && pop) {
            m_pop = pop;
        }
    }

protected: /* functions */
    GeneticTool() :
        utils::Singleton<T>(),
        m_pop(0)
    {}

protected: /* variables */
    Population *m_pop;
};

} // namespace ga

#endif // H_GENETICTOOL
