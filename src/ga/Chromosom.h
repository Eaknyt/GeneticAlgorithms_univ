#ifndef H_CHROMOSOM
#define H_CHROMOSOM

#include <iosfwd>
#include <vector>

#include "ga/Gene.h"

namespace ga {

class Chromosom
{
private:
	static int m_nextName;
	int m_name;
	std::vector<Gene *> m_genes;

public:
	Chromosom();
	Chromosom(const std::vector<Gene *> &genes);
	Chromosom(const Chromosom &other);
	virtual ~Chromosom();

	int getName() const;

	virtual float fitness() const = 0;

	std::size_t getLength() const;

	void insert(Gene *gene);
	void remove(Gene *gene);

	std::vector<Gene *> getGenes() const;
    std::vector<Gene *> &getGenes_ref();

	bool containsGene(Gene *gene) const;

    virtual void print(std::ostream &os) const = 0;

    friend std::ostream &operator<<(std::ostream &os, const Chromosom &chr);
};

} // namespace ga

#endif // H_CHROMOSOM
