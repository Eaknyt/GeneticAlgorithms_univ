#ifndef H_CHR_FACTORY
#define H_CHR_FACTORY

#include <vector>

namespace ga {

class Chromosom;

/**
 * An class abstraction of a the Factory pattern for Chromosoms.
 */
 class ChrFactory
{
public:
    ChrFactory();
    virtual ~ChrFactory();

    virtual Chromosom *create() const = 0;
    virtual ga::Chromosom *createRandom(int geneCount) const = 0;
    virtual std::vector<ga::Chromosom *> createVariations(int geneCount,
                                                          int count) const = 0;

    bool release(Chromosom *chr) const;
};

} // namespace ga

#endif // H_CHR_FACTORY