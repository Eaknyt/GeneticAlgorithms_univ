#include "Mutator.h"

#include <algorithm>
#include <assert.h>
#include <iterator>

#include "ga/Chromosom.h"
#include "ga/Gene.h"

namespace ga {

/**
 * Apply a random mutation on 'pr' chromosoms in 'chrs. Returns the mutated
 * chromosoms.
 */
std::vector<Chromosom *> Mutator::mutate(const std::vector<Chromosom *> &chrs,
                                         unsigned int pr) const
{
    assert(pr <= 100);

    std::vector<Chromosom *> toMutate = utils::randomPart(chrs, pr);
    std::vector<Chromosom *>::const_iterator it = toMutate.begin();

    while (it != toMutate.end()) {
        Chromosom *c = (*it);

        int cSize = c->getLength();
        
        int idx01 = utils::randomInt(0, cSize);
        int idx02 = idx01;

        while (idx02 == idx01)
            idx02 = utils::randomInt(0, cSize);

        // Retrieve the genes
        std::vector<Gene *> &genes = c->getGenes_ref();

        // Get iterators to the indexes generated above
        std::vector<Gene *>::iterator first = genes.begin();
        std::vector<Gene *>::iterator second = first;
        
        std::advance(first, idx01);
        std::advance(second, idx02);

        // Apply mutation
        std::swap(*first, *second);

        ++it;
    }

    return toMutate;
}

} // namespace ga
