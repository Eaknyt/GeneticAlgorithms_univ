#ifndef H_POPULATION
#define H_POPULATION

#include <vector>

#include "ga/Chromosom.h"

namespace ga {

class Population
{
public:
    Population();
    Population(const std::vector<Chromosom *> &chromosoms);
    ~Population();

    int getName() const;

    void addChromosom(Chromosom *chromosom);
    void addChromosoms(const std::vector<Chromosom *> &chrs);
    void removeChromosom(Chromosom *chromosom);

    std::vector<Chromosom *> getChromosoms() const;
    std::size_t getCount() const;

    bool contains(Chromosom *chr) const;

private:
	static int m_nextName;
	int m_name;
	std::vector<Chromosom *> m_chromosoms;
};

} // namespace ga

#endif // H_POPULATION
