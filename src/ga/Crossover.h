#ifndef H_CROSSOVER
#define H_CROSSOVER

#include "ga/GeneticTool.h"

namespace ga {

class Chromosom;

class Crossover : public GeneticTool<Crossover> 
{
public:
    enum CrossTechnique
    {
        OnePointCross,
        OnePointCrossOrdered,
        ShuffleCross
    };

    void cross(Chromosom *father, Chromosom *mother,
               Chromosom *sibling1, Chromosom *sibling2,
               Crossover::CrossTechnique technique) const;

private:
    void cross_onePoint(Chromosom *father, Chromosom *mother,
                        Chromosom *s1, Chromosom *s2) const;

    void cross_onePointOrdered(Chromosom *father, Chromosom *mother,
                               Chromosom *s1, Chromosom *s2) const;

    void cross_shuffle(Chromosom *father, Chromosom *mother,
                       Chromosom *s1, Chromosom *s2) const;
};

} // namespace ga

#endif // H_CROSSOVER
