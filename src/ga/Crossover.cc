#include "Crossover.h"

#include <assert.h>
#include <vector>

#include "ga/Chromosom.h"
#include "ga/Gene.h"


//////////////////////// Helpers ////////////////////////

namespace {

/**
 * Ends to fill the chromosom 'sibling' with its parent genes 'parentGenes'.
 *
 *
 */
void endsFillSibling(std::size_t crossIndex, std::size_t size,
                     ga::Chromosom *sibling,
                     std::vector<ga::Gene *> &parentGenes)
{
    std::size_t parentIdx = crossIndex;

    while (sibling->getLength() != size) {
        ga::Gene *geneFromParent = parentGenes[parentIdx];

        if (sibling->containsGene(geneFromParent)) {
            if (parentIdx != (size - 1))
                ++parentIdx;
            else
                parentIdx = 0;
        }
        else {
            sibling->insert(geneFromParent);

            if (parentIdx != (size - 1))
                ++parentIdx;
            else
                parentIdx = 0;
        }
    }    
}

} // anon namespace 


namespace ga {


//////////////////////// Crossover ////////////////////////

    /**
     * @brief Performs a crossover on 'parents', using the crossover technique
     * 'technique'.
     */
    void Crossover::cross(Chromosom *father, Chromosom *mother,
                          Chromosom *sibling1, Chromosom *sibling2,
                          Crossover::CrossTechnique technique) const 
    {
        // Check that the two chromosoms have the same size
        assert(father->getLength() == mother->getLength());

        // Check that the two siblings are empty
        assert(!sibling1->getLength() && !sibling2->getLength());

        // Perform the crossover using the requested technique
        switch (technique) {
        case Crossover::OnePointCross:
            cross_onePoint(father, mother, sibling1, sibling2);
            break;
        case Crossover::OnePointCrossOrdered:
            cross_onePointOrdered(father, mother, sibling1, sibling2);
            break;
        case Crossover::ShuffleCross:
            cross_shuffle(father, mother, sibling1, sibling2);
        default:
            break;
        }

        assert(sibling1->getLength() == sibling2->getLength());
        assert(sibling1->getLength() == father->getLength());
    }

    /**
     * @brief Performs a one-point crossover on 'parents', adding the result
     * genes to 's1' and 's2'.
     *
     * This is a very basic crossover function. It does not support ordered
     * chromosoms, such as an ordered list of towns. Thus it does not suit the
     * travelling salesman problem.
     *
     * This function assumes that 'father' and 'mother' have same lengths, and
     * that 's1' and 's2' are empty chromosoms.
     */
    void Crossover::cross_onePoint(Chromosom *father, Chromosom *mother,
                                   Chromosom *s1, Chromosom *s2) const
    {
        const std::size_t length = father->getLength();

        // Select a random cross index
        std::size_t crossIndex = utils::randomInt(0, length);

        std::vector<Gene *> fatherGenes = father->getGenes();
        std::vector<Gene *> motherGenes = mother->getGenes();

        for (std::size_t i = 0; i < crossIndex; ++i) {
            s1->insert(fatherGenes[i]);
            s2->insert(motherGenes[i]);
        }

        for (std::size_t i = crossIndex; i < length; ++i) {
            s1->insert(motherGenes[i]);
            s2->insert(fatherGenes[i]);
        }
    }

    /**
     * @brief Performs a one-point crossover on 'parents', adding the result
     * genes to 's1' and 's2'.
     *
     * Ordered chromosoms are supported.
     *
     * This function assumes that 'father' and 'mother' have same lengths, and
     * that 's1' and 's2' are empty chromosoms.
     */
    void Crossover::cross_onePointOrdered(Chromosom *father, Chromosom *mother,
                                          Chromosom *s1, Chromosom *s2) const
    {
        const std::size_t length = father->getLength();

        // Select a random cross index
        std::size_t crossIndex = utils::randomInt(0, length);

        std::vector<Gene *> fatherGenes = father->getGenes();
        std::vector<Gene *> motherGenes = mother->getGenes();

        for (std::size_t i = 0; i < crossIndex; ++i) {
            s1->insert(fatherGenes[i]);
            s2->insert(motherGenes[i]);
        }

        endsFillSibling(crossIndex, length, s1, motherGenes);
        endsFillSibling(crossIndex, length, s2, fatherGenes);
    }

    void Crossover::cross_shuffle(Chromosom *father, Chromosom *mother,
                                 Chromosom *s1, Chromosom *s2) const
    {
        std::vector<Gene *> fatherGenes = father->getGenes();
        std::vector<Gene *> motherGenes = mother->getGenes();

        std::random_shuffle(fatherGenes.begin(), fatherGenes.end());
        std::random_shuffle(motherGenes.begin(), motherGenes.end());

        std::vector<Gene *>::const_iterator it = fatherGenes.begin();        
        
        while (it != fatherGenes.end()) {
            s2->insert(*it);
            ++it;
        }

        it = motherGenes.begin();        
        
        while (it != motherGenes.end()) {
            s1->insert(*it);
            ++it;
        }
    }

} // namespace ga
