#include "Gene.h"
#include <iostream>
namespace ga {

int Gene::m_nextName = 0;

Gene::Gene() :
	m_name(m_nextName)
{	
	m_nextName++;
}

Gene::Gene(const Gene &other) :
    m_name(other.m_name)
{}

Gene::~Gene() {
}

int Gene::getName() const
{
	return m_name;
}

bool Gene::compare(Gene *first, Gene *second) const
{
    return false;
}

bool Gene::operator==(Gene &other)
{
    return compare(this, &other);
}

} // namespace ga
