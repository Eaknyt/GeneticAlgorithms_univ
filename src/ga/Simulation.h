#ifndef H_SIMULATION
#define H_SIMULATION

#include "ga/Crossover.h"

namespace ga {

class ChrFactory;
class Population;

class Simulation
{
public:
    Simulation();
    virtual ~Simulation();

    virtual Chromosom *start();
    void next();

    Chromosom *currentBest() const;

    unsigned int iterationCount() const;
    void setIterationCount(unsigned int count);

    unsigned int individualCount() const;
    void setIndividualCount(unsigned int count);

    unsigned int blockElementCount() const;
    void setBlockElementCount(unsigned int count);

    unsigned int elitismPr() const;
    void setElitismPr(unsigned int pr);

    Crossover::CrossTechnique crossoverTechnique() const;
    void setCrossoverTechnique(Crossover::CrossTechnique tech);

    unsigned int crossoverPr() const;
    void setCrossoverPr(unsigned int pr);

    unsigned int mutationPr() const;
    void setMutationPr(unsigned int pr);

protected:
    Population *m_initialPop;
    ChrFactory *m_factory;

private: /* functions */
    void checkParams();
    void updateOperators();
    void cleanupPreviousPopulation(Population *newPop);

private: /* variables */
    unsigned int m_iterationCount;
    unsigned int m_individualCount;
    unsigned int m_blockElementCount;

    unsigned int m_elitismPr;
    Crossover::CrossTechnique m_crossoverTech;
    unsigned int m_crossoverPr;
    unsigned int m_mutationPr;
};

} // namespace ga

#endif // H_SIMULATION
