#ifndef H_ELITIST
#define H_ELITIST

#include "ga/GeneticTool.h"

namespace ga {

class Chromosom;

class Elitist : public GeneticTool<Elitist>
{
public:
    std::vector<Chromosom *> select(unsigned int pr) const;
};

} // namespace ga

#endif // H_ELITIST
