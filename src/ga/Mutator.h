#ifndef H_MUTATOR
#define H_MUTATOR

#include "ga/GeneticTool.h"

namespace ga {

class Chromosom;

class Mutator  : public GeneticTool<Mutator>
{
public:
    std::vector<Chromosom *> mutate(const std::vector<Chromosom *> &chrs,
                                    unsigned int pr) const;
};

} // namespace ga

#endif // H_MUTATOR
