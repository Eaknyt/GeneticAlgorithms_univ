#include "ChrFactory.h"

#include "ga/Chromosom.h"

namespace ga {

ChrFactory::ChrFactory()
{}

ChrFactory::~ChrFactory()
{}

bool ChrFactory::release(Chromosom *chr) const
{
    bool ret = bool(chr);

    if (chr)
        delete chr;

    return ret;
}

} // namespace ga
