#ifndef H_GENE
#define H_GENE

namespace ga {

class Gene {
private:
	static int m_nextName;
	int m_name;
		
public:
	Gene();
    Gene(const Gene &other);
	virtual ~Gene();
	
	int getName() const;

    bool operator==(Gene &other);

protected:
    virtual bool compare(Gene *first, Gene *second) const;
};

} // namespace ga

#endif // H_GENE
