#include "DrawWrapper.h"

#include <iostream>
#include <vector>

#include "ga/Gene.h"
#include "ga/Chromosom.h"

#include "graph/CClientTCP.h"

#include "tsp/Path.h"
#include "tsp/Town.h"

#include "utils/Point.h"

namespace app {

DrawWrapper::DrawWrapper() :
    m_bbInterface(new CClientTCP("127.0.0.1", 1500))
{
    clear();
}

DrawWrapper::~DrawWrapper()
{
    delete m_bbInterface;
}

void DrawWrapper::clear()
{
    m_bbInterface->clear();
}

void DrawWrapper::drawTown(tsp::Town *town)
{
    utils::Point coords = town->getCoords();

    m_bbInterface->drawCircle(coords.x(), coords.y(), 7, CColor::sBlue, true);
}

void DrawWrapper::drawPath(ga::Chromosom *path, bool drawSegments)
{
    std::vector<ga::Gene *> towns = path->getGenes();
    std::vector<ga::Gene *>::const_iterator it = towns.begin();

    // Draw the first town
    tsp::Town *firstTown = dynamic_cast<tsp::Town *>(*it);
    utils::Point firstCoords = firstTown->getCoords();
    
    drawTown(firstTown);

    ++it;

    // Draw paths and next towns

    while (it != towns.end()) {
        tsp::Town *t = dynamic_cast<tsp::Town *>(*it);
        drawTown(t);

        tsp::Town *previous = dynamic_cast<tsp::Town *>(*(it - 1));

        utils::Point coords = t->getCoords();
        utils::Point previousCoords = previous->getCoords();

        if (drawSegments) {
            m_bbInterface->drawSegment(coords.x(), coords.y(),
                                       previousCoords.x(), previousCoords.y(),
                                       CColor::sWhite);
        }
        ++it;
    }

    // Close the path
    tsp::Town *lastTown = dynamic_cast<tsp::Town *>(*(towns.end() - 1));
    utils::Point lastCoords = lastTown->getCoords();

    if (drawSegments) {
        m_bbInterface->drawSegment(firstCoords.x(), firstCoords.y(),
                                   lastCoords.x(), lastCoords.y(),
                                   CColor::sWhite);
    }
}

void DrawWrapper::end()
{
    m_bbInterface->disconnect();
}

} // namespace app
