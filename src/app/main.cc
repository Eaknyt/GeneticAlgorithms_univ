#include <cstdlib>
#include <iostream>
#include <string>

#include "app/DrawWrapper.h"

#include "graph/CClientTCP.h"

#include "tsp/Path.h"
#include "tsp/PathFactory.h"
#include "tsp/TspSimulation.h"

#include "utils/utilsfuncs.h"

/**
 * Usage :
 *
 * - Start Blackboard.
 * - Connect it to the port 1500.
 * - Execute <buildFolder>/bin/main.exe <iterations> <individualCount> <geneCount>.
 */
int main(int argc, char **argv)
{
    if (argc != 4) {
        std::cerr << "Usage : " << argv[0]
                  <<" <iterations> <individualCount> <geneCount>"
                  << std::endl;
        return 1;
    }

    unsigned int iterCount = atoi(argv[1]);
    unsigned int individualCount = atoi(argv[2]);
    unsigned int geneCount = atoi(argv[3]);

    // Init the random engine
    utils::initRandEngine();

    // Try to connect to Blackboard
    app::DrawWrapper dw;

    // Create a tsp simulation
    tsp::TspSimulation sim;
    sim.setIterationCount(iterCount);
    sim.setIndividualCount(individualCount);
    sim.setBlockElementCount(geneCount);
    sim.setElitismPr(10);
    sim.setCrossoverPr(20);
    sim.setCrossoverTechnique(ga::Crossover::ShuffleCross);
    sim.setMutationPr(20);

    // Start simulation
    ga::Chromosom *cities = sim.start();
    dw.drawPath(cities, false);

    ga::Chromosom *best = 0;

    for (unsigned int i = 0; i < sim.iterationCount(); ++i) {
        sim.next();

        best = sim.currentBest();
        std::cout << "Iteration " << i << ", best fitness : "
                  << best->fitness() << std::endl;
    }

    // Display the result in Blackboard
    dw.drawPath(cities, true);

    dw.end();

    return 0;
}
