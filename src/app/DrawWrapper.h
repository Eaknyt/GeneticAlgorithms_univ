#ifndef H_DRAW_WRAPPER
#define H_DRAW_WRAPPER

namespace ga {
class Chromosom;
} // namespace ga

namespace tsp {
class Town;
} // namespace tsp

class CClientTCP;

namespace app {

class DrawWrapper
{
public:
    DrawWrapper();
    ~DrawWrapper();

    void clear();

    void drawTown(tsp::Town *town);
    void drawPath(ga::Chromosom *path, bool drawSegments);

    void end();

private:
    CClientTCP *m_bbInterface;
};

} // namsepace app

#endif // H_DRAW_WRAPPER