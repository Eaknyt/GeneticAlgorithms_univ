#include "Path.h"

#include <iostream>

#include "tsp/tspfuncs.h"

#include "utils/Point.h"

namespace tsp {

Path::Path() :
    ga::Chromosom()
{
}

Path::Path(const std::vector<ga::Gene *> &v) :
    ga::Chromosom(v)
{
}

Path::~Path()
{
}

float Path::fitness() const
{
    float ret = 0.0f;

    std::vector<ga::Gene *> myGenes = getGenes();

    std::vector<ga::Gene *>::const_iterator it = myGenes.begin() + 1;

    while (it != myGenes.end()) {
        Town *currentTown = dynamic_cast<Town *>(*it);
        Town *previousTown = dynamic_cast<Town *>(*(it - 1));

        float dist = tsp::distance(currentTown->getCoords(),
                                   previousTown->getCoords());

        ret += dist;

        ++it;
    }

    return ret;
}

void Path::print(std::ostream &os) const
{
    os << "tsp::Path(" << std::endl;

    std::vector<ga::Gene *> towns = getGenes();

    std::vector<ga::Gene *>::const_iterator it = towns.begin();
    std::vector<ga::Gene *>::const_iterator end = towns.end();

    while (it != end) {
        Town *t = dynamic_cast<Town *>(*it);
        utils::Point coords = t->getCoords();

        os << "\t(" << coords.x() << ", " << coords.y() << ")";

        if (it != (end - 1)) {
            os << "," << std::endl;
        }
        else {
            os << std::endl << ")";
        }

        ++it;
    }
}

} // namespace tsp
