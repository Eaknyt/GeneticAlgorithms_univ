#include "tspfuncs.h"

#include <cmath>

#include "utils/Point.h"

namespace tsp {

float distance(const utils::Point &p1, const utils::Point &p2)
{
    return std::sqrt(std::pow(p2.x() - p1.x(), 2) +
                     std::pow(p2.y() - p1.y(), 2));
}

} // namespace tsp
