#ifndef H_PATH
#define H_PATH

#include "ga/Chromosom.h"

#include <iosfwd>
#include <vector>

#include "ga/Gene.h"
#include "tsp/Town.h"

namespace tsp {

class Path : public ga::Chromosom 
{
public:
	Path();
	Path(const std::vector<ga::Gene *> &v);
	~Path();

    float fitness() const;

    void print(std::ostream &os) const;
};

} // namespace tsp

#endif // H_PATH
