#ifndef H_TSPFUNCS
#define H_TSPFUNCS

namespace utils {
class Point;
}

namespace tsp {

float distance(const utils::Point &p1, const utils::Point &p2);

} // namespace tsp

#endif // H_TSPFUNCS