#include "PathFactory.h"

#include <algorithm>
#include <assert.h>

#include "ga/Chromosom.h"

#include "tsp/Path.h"

#include "utils/utilsfuncs.h"

namespace tsp {

PathFactory::PathFactory() :
    ga::ChrFactory(),
    m_left(0),
    m_top(0),
    m_right(1500),
    m_bottom(820)
{}

PathFactory::~PathFactory()
{}

/**
 * @brief Returns the area in which the paths generated with ::createRandom
 * will take place.
 */
void PathFactory::area(int *left, int *top, int *right, int *bottom)
{
    assert(left);
    assert(top);
    assert(right);
    assert(bottom);

    *left = m_left;
    *top = m_top;
    *right = m_right;
    *bottom = m_bottom;
}

/**
 * @brief Set the area in which the paths generated with ::createRandom will take
 * place.
 */
void PathFactory::setArea(int left, int top, int right, int bottom)
{
    m_left = left;
    m_top = top;
    m_right = right;
    m_bottom = bottom;
}

/**
 * @brief Creates and returns a default Path, which means that every Town into it have
 * (0, 0) coordinates.
 */
ga::Chromosom *PathFactory::create() const
{
    return new Path();
}

/**
 * @brief Creates and returns a Path of 'geneCount' towns with random
 * coordinates within the rectangle defined by ::area().
 */
ga::Chromosom *PathFactory::createRandom(int geneCount) const
{
    ga::Chromosom *ret = create();
    
    for (int i = 0; i < geneCount; ++i) {
        utils::Point coords = utils::randomPoint(m_left, m_top,
                                                 m_right, m_bottom);
        
        ret->insert(new Town(coords));
    }

    return ret;
}

/**
 * @brief Creates 'count' variations of the same path, which have 'geneCount'
 * towns.
 */
std::vector<ga::Chromosom *> PathFactory::createVariations(int geneCount, 
                                                           int count) const
{
    std::vector<ga::Chromosom *> ret;

    ga::Chromosom *first = createRandom(geneCount);
    ret.push_back(first);

    for (int i = 0; i < count; ++i) {
        std::vector<ga::Gene *> shuffled = first->getGenes();
        std::random_shuffle(shuffled.begin(), shuffled.end());

        ga::Chromosom *variation = new Path(shuffled);

        ret.push_back(variation);
    }

    return ret;
}

} // namespace tsp
