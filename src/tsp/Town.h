#ifndef H_TOWN
#define H_TOWN

#include "ga/Gene.h"

#include "utils/Point.h"

namespace tsp {

class Town : public ga::Gene 
{
private:
	utils::Point m_coords;

public:
	Town();
	Town(const utils::Point &coords);
    Town(int x, int y);
    Town(const Town &other);
	~Town();

    utils::Point getCoords() const;
    void setCoords(const utils::Point &coords);

protected:
    bool compare(Gene *first, Gene *second) const;
};

} // namespace tsp

#endif // H_TOWN	
