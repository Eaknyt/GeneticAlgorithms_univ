#include "Town.h"

namespace tsp {

Town::Town() :
    ga::Gene(),
    m_coords()
{
}

Town::Town(const utils::Point &coords) :
    ga::Gene(),
    m_coords(coords)
{
}

Town::Town(const Town &other) :
    ga::Gene(other),
    m_coords(other.m_coords)
{}

Town::Town(int x, int y) :
    ga::Gene(),
    m_coords(utils::Point(x, y))
{}

Town::~Town() 
{}

utils::Point Town::getCoords() const
{
    return m_coords;
}

void Town::setCoords(const utils::Point &coords)
{
    if (m_coords != coords)
        m_coords = coords;
}

bool Town::compare(Gene *first, Gene *second) const
{
    Town *firstt = dynamic_cast<Town *>(first);
    Town *secondt = dynamic_cast<Town *>(second);

    return (firstt->m_coords == secondt->m_coords);
}

} // namespace tsp
