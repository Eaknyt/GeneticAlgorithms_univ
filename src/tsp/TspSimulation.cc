#include "tsp/TspSimulation.h"

#include "tsp/PathFactory.h"

namespace tsp {

TspSimulation::TspSimulation() :
    ga::Simulation()
{
    m_factory = new tsp::PathFactory;
}

} // namespace tsp
