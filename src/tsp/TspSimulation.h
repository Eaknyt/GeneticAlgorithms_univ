#ifndef H_TSPSIMULATION
#define H_TSPSIMULATION

#include "ga/Simulation.h"

namespace tsp {

class TspSimulation : public ga::Simulation
{
public:
    TspSimulation();
};

} // namespace tsp

#endif // H_TSPSIMULATION