#ifndef H_PATH_FACTORY
#define H_PATH_FACTORY

#include "ga/ChrFactory.h"

namespace tsp {

class Path;

class PathFactory : public ga::ChrFactory
{
public:
    PathFactory();
    ~PathFactory();

    ga::Chromosom *create() const;
    ga::Chromosom *createRandom(int geneCount) const;
    std::vector<ga::Chromosom *> createVariations(int geneCount,
                                                  int count) const;

    void area(int *left, int *top, int *right, int *bottom);
    void setArea(int left, int top, int right, int bottom);

private:
    // Generation rectangle
    int m_left, m_top, m_right, m_bottom;
};

} // namespace tsp

#endif // H_PATH_FACTORY