#ifndef H_SINGLETON
#define H_SINGLETON

namespace utils {

/**
 * A basic singleton template class.
 */
template <class T>
class Singleton
{
public:
    virtual ~Singleton()
    {}

    static T *instance()
    {
        if (!m_instance)
            m_instance = new T;

        return m_instance;
    }

protected:
    Singleton()
    {}

private:
    static T *m_instance;
};

template <class T> T *Singleton<T>::m_instance = 0;

} // namespace utils

#endif // H_SINGLETON
