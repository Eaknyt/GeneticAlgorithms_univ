#ifndef H_UTILSFUNCS
#define H_UTILSFUNCS

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

#include "Point.h"

namespace utils {

void initRandEngine();
int randomInt(int min, int max);
bool toss(unsigned int pr);
void randomSubRange(int size, int *lower, int *upper, int subSize);

Point randomPoint(int left, int top, int right, int bottom);

int percentage(int size, int pr);

/**
 * Returns a shuffled sub vector from 'values'.
 *
 * 'pr' defines the size of the new vector.
 */
template <class T>
std::vector<T> randomPart(const std::vector<T> values, int pr)
{
    std::vector<T> v = values;
    std::random_shuffle(v.begin(), v.end());

    int count = percentage(v.size(), pr);

    return std::vector<T>(v.begin(), v.begin() + count);
}

} // namespace utils

#endif // H_UTILSFUNCS