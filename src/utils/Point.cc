#include "Point.h"

#include <iostream>

namespace utils {

Point::Point() :
    m_x(0),
    m_y(0) {}

Point::Point(int x, int y) :
    m_x(x),
    m_y(y) {}

Point::Point(const Point &other) :
    m_x(other.m_x),
    m_y(other.m_y) {}

int Point::x() const {
    return m_x;
}

int Point::y() const {
    return m_y;
}

int &Point::rx() {
    return m_x;
}

int &Point::ry() {
    return m_y;
}

bool Point::operator==(const Point &other)
{
    if (m_x != other.m_x || m_y != other.m_y)
        return false;

    return true;
}

bool Point::operator!=(const Point &other)
{
    return !(*this == other);
}

std::ostream &operator<<(std::ostream &os, const Point &point)
{
    os << "utils::Point(" << point.m_x << ", " << point.m_y <<  ")";

    return os;
}

} // namespace utils
