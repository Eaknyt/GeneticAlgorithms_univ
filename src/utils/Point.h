#ifndef H_POINT
#define H_POINT

#include <iosfwd>

namespace utils {

class Point
{
private:
    int m_x;
    int m_y;

public:
    Point();
    Point(int x, int y);
    Point(const Point &other);

    int x() const;
    int &rx();

    int y() const;
    int &ry();

    bool operator==(const Point &other);
    bool operator!=(const Point &other);

    friend std::ostream &operator<<(std::ostream &os, const Point &point);
};

} // namespace utils

#endif // H_POINT
