#include "utilsfuncs.h"

#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include <time.h>

namespace utils {

/**
 * @brief Initialize the srand engine. Does nothing if it is already
 * initialized.
 */
void initRandEngine()
{
    static bool initialized = false;

    if (!initialized) {
        std::srand(time(NULL));

        initialized = true;
    }
}

/**
 * @brief Returns a random integer between 'min' and 'max'.
 *
 * http://stackoverflow.com/questions/7560114/random-number-c-in-some-range
 * //TODO update with C++11 features
 */
int randomInt(int min, int max)
{
    return min + std::rand() % (max - min);
}

/**
 * @brief Returns true if an dummy event with the probability 'pr' happens,
 * otherwise returns false.
 */
bool toss(unsigned int pr)
{
    return static_cast<unsigned int>(rand() % 100) < pr;
}

/**
 * @brief Generates randomly two bounds within the range [0, 'size'], and put
 * the results in 'lower' and 'upper'.
 *
 * You can provide a minimum size the generated range must have. If 'subSize'
 * is equals to -1, there's no size check.
 */
void randomSubRange(int size, int *lower, int *upper, int subSize)
{
    assert(subSize >= 1);
    assert(subSize <= size);

    // Generate the first bound
    int l = randomInt(0, size);

    int u = -1;

    if ( (size - l) <= subSize) {
        u = l;
        l -= subSize;
    }
    else {
        u = l + subSize;
    }

    assert((u - l) == subSize);

    // Return the results
    *lower = l;
    *upper = u;
}

/**
 * @brief Returns a random point in the rectangle defined by
 * (left, top, right, bottom).
 *
 * The origin is (left, top) and the rectangle has rightward x-axis and
 * downward y-axis.
 *
 * Bounds can be positive or negative integers.
 */
Point randomPoint(int left, int top, int right, int bottom)
{
    assert(left < right);
    assert(top < bottom);

    int x = randomInt(left, right);
    int y = randomInt(top, bottom);

    return Point(x, y);
}

/**
 *
 */
int percentage(int size, int pr)
{
    int ret = (size * pr) / 100;

    // Round the result if it's < 1
    if (!ret)
        ret = 1;

    return ret;
}

} // namespace utils
