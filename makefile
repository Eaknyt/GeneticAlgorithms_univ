all: lib samples tests app

lib: Utils GA TSP Graph
samples: sample_singleton sample_mutator sample_blackboard
tests: tests_elitist tests_crossover

# App
app: Graph Utils GA TSP
	@$(foreach ccFile, $(APP_CC_FILES), $(APP_BUILD_CLASS_CMD);)

	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(BIN_PATH)main.exe \
		 $(GRAPH_OBJ) $(UTILS_OBJ) $(GA_OBJ) $(TSP_OBJ) $(APP_OBJ)

APP_CC_FILES = DrawWrapper \
			   main

APP_BUILD_CLASS_CMD = $(CC) $(CC_FLAGS) $(INC_PATH) \
	-o $(OBJ_PATH)$(ccFile).o -c $(APP_PATH)$(ccFile).cc

APP_OBJ = $(foreach ccFile, $(APP_CC_FILES), \
	$(OBJ_PATH)$(ccFile).o)


# Samples
sample_singleton: Utils
	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(SAMPLES_BUILD_PATH)$@.exe $(UTILS_OBJ) \
		$(SAMPLES_PATH)$@.cc

sample_mutator: Utils GA TSP
	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(SAMPLES_BUILD_PATH)$@.exe $(UTILS_OBJ) \
		$(GA_OBJ) $(TSP_OBJ) $(SAMPLES_PATH)$@.cc

sample_blackboard: Graph Utils GA TSP
	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(SAMPLES_BUILD_PATH)$@.exe $(GRAPH_OBJ) \
		$(UTILS_OBJ) $(GA_OBJ) $(TSP_OBJ) $(SAMPLES_PATH)$@.cc


# Tests

# Whenever you add a test file, add its name here
TESTS_CC_FILES = tests_elitist tests_crossover

tests_elitist: Utils GA TSP
	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(TESTS_BUILD_PATH)$@.exe \
	$(UTILS_OBJ) $(GA_OBJ) $(TSP_OBJ) $(TESTS_PATH)$@.cc

tests_crossover: Utils GA TSP
	$(CC) $(CC_FLAGS) $(INC_PATH) -o $(TESTS_BUILD_PATH)$@.exe \
	$(UTILS_OBJ) $(GA_OBJ) $(TSP_OBJ) $(TESTS_PATH)$@.cc

run_tests: build_tests
	@$(foreach testFile, $(TESTS_CC_FILES), $(TESTS_RUN_CMD);)

TESTS_RUN_CMD = $(TESTS_BUILD_PATH)$(testFile).exe


# Source files

#  Add your filenames for the GA package here
GA_CC_FILES = Elitist \
			  Crossover \
			  Mutator \
			  Population \
			  Chromosom \
			  Gene \
			  ChrFactory \
			  Simulation

#  Add your filenames for the TSP package here
TSP_CC_FILES = Path \
			   Town \
			   PathFactory \
			   TspSimulation \
			   tspfuncs

#  Add your filenames for the Graph package here
GRAPH_CPP_FILES = CClientTCP \
				  CColor

#  Add your filenames for the Utils package here
UTILS_CC_FILES = Point \
				 utilsfuncs


# Tools
#  Build the output directory
init:
	mkdir -p $(BUILD_PATH)
	mkdir -p $(OBJ_PATH)
	mkdir -p $(BIN_PATH)
	mkdir -p $(SAMPLES_BUILD_PATH)
	mkdir -p $(TESTS_BUILD_PATH)

#  Remove object files
clean:
	rm -f $(OBJ_PATH)*.o

#  Remove temporary files AND executable files
purge: clean
	rm -f $(BIN_PATH)*.exe $(SAMPLES_BUILD_PATH)*.exe $(TESTS_BUILD_PATH)*.exe

# Use it once the project is finished
fake_install: clean
	rmdir $(OBJ_PATH)


# Global constants
CC = g++
CC_FLAGS = -g -Wall

#  Sources paths
SRC_PATH = src/
UTILS_PATH = src/utils/
GRAPH_PATH = src/graph/
GA_PATH = src/ga/
TSP_PATH = src/tsp/
APP_PATH = src/app/
SAMPLES_PATH = samples/
TESTS_PATH = tests/

#  Build directories paths
BUILD_PATH = ../build/
OBJ_PATH = $(BUILD_PATH)obj/
BIN_PATH = $(BUILD_PATH)bin/
SAMPLES_BUILD_PATH = $(BUILD_PATH)samples/
TESTS_BUILD_PATH = $(BUILD_PATH)tests/

# Global include path
INC_PATH = -I$(SRC_PATH)


# --------------------------------------------

# /!\ Make sure to know what you're doing


# Packages

#  GA (Genetic Algorithms) package
GA: Utils
	@$(foreach ccFile, $(GA_CC_FILES), $(GA_BUILD_CLASS_CMD);)

GA_BUILD_CLASS_CMD = $(CC) $(CC_FLAGS) -I$(SRC_PATH) -I$(UTILS_PATH) \
-o $(OBJ_PATH)$(ccFile).o -c $(GA_PATH)$(ccFile).cc

GA_OBJ = $(foreach ccFile, $(GA_CC_FILES), \
	$(OBJ_PATH)$(ccFile).o)

#  TSP (Travelling Salesman Problem) package
TSP: GA Utils
	@$(foreach ccFile, $(TSP_CC_FILES), $(TSP_BUILD_CLASS_CMD);)

TSP_BUILD_CLASS_CMD = $(CC) $(CC_FLAGS) -I$(SRC_PATH) $(INC_PATH) \
	-o $(OBJ_PATH)$(ccFile).o -c $(TSP_PATH)$(ccFile).cc

TSP_OBJ = $(foreach ccFile, $(TSP_CC_FILES), \
	$(OBJ_PATH)$(ccFile).o)

#  Graph package
Graph:
	@$(foreach cppFile, $(GRAPH_CPP_FILES), $(GRAPH_BUILD_CLASS_CMD);)

GRAPH_BUILD_CLASS_CMD = $(CC) $(CC_FLAGS) -I$(SRC_PATH) -o $(OBJ_PATH)$(cppFile).o \
	-c $(GRAPH_PATH)$(cppFile).cpp

GRAPH_OBJ = $(foreach cppFile, $(GRAPH_CPP_FILES), $(OBJ_PATH)$(cppFile).o)

#  Utils package
Utils:
	@$(foreach ccFile, $(UTILS_CC_FILES), $(UTILS_BUILD_CLASS_CMD);)

UTILS_BUILD_CLASS_CMD = $(CC) $(CC_FLAGS) -I$(SRC_PATH) -o $(OBJ_PATH)$(ccFile).o \
	-c $(UTILS_PATH)$(ccFile).cc

UTILS_OBJ = $(foreach ccFile, $(UTILS_CC_FILES), $(OBJ_PATH)$(ccFile).o)
