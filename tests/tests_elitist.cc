#include <assert.h>

#include "ga/Elitist.h"
#include "ga/Population.h"

#include "tsp/Path.h"
#include "tsp/PathFactory.h"
#include "tsp/Town.h"

/**
 * Test file for the ga::Elitist class
 * -----------------------------------
 *
 * /!\ No unit testing library is used.
 *
 * The tests are executed using the assert function declared in assert.h.
 * So basically, if there's no output when you run the program, that's
 * considered successfull.
 */

/**
 * @brief Returns true if the chromosom 'best' has a greater or equal fitness
 * than each chromosom contained in 'others'.
 *
 * If 'best' is already contained in 'others', there's no check between itself.
 *
 * /!\ Remember that we work on paths. So here, a greater fitness value means a
 * worse fitness.
 */
bool chrIsBetter(ga::Chromosom *best, std::vector<ga::Chromosom *> *others)
{
    bool ret = true;

    std::vector<ga::Chromosom *>::const_iterator it = others->begin();
    
    while (it != others->end()) {
        ga::Chromosom *chr = (*it);

        if (best == chr) {
            ++it;
            continue;
        }

        if (best->fitness() > chr->fitness()) {
            ret = false;
            break;
        }

        ++it;
    }

    return ret;
}

/**
 * Instanciate some specialized chromosoms and genes, performs an elitism pass 
 * and check that the best individuals are effectively selected.
 */
int main()
{
    tsp::PathFactory factory;
    ga::Chromosom *c1 = factory.createRandom(3);
    ga::Chromosom *c2 = factory.createRandom(3);

    ga::Population pop;
    pop.addChromosom(c1);
    pop.addChromosom(c2);

    ga::Elitist *elitist = ga::Elitist::instance();
    elitist->assign(&pop);
    std::vector<ga::Chromosom *> bestIndividuals = elitist->select(10);

    // Check that each selected individual is better than the others
    std::vector<ga::Chromosom *>::const_iterator it = bestIndividuals.begin();
    
    while (it != bestIndividuals.end()) {
        ga::Chromosom *best = (*it);

        std::vector<ga::Chromosom *> individuals = pop.getChromosoms();

        assert(chrIsBetter(best, &individuals));

        ++it;
    }

    return 0;
}
