#include <assert.h>

#include "ga/Chromosom.h"
#include "ga/Crossover.h"

#include "tsp/Path.h"
#include "tsp/PathFactory.h"
#include "tsp/Town.h"

#include "utils/utilsfuncs.h"

/**
 * Test file for the ga::Crossover class
 * -------------------------------------
 *
 * /!\ No unit testing library is used.
 *
 * The tests are executed using the assert function declared in assert.h.
 * So basically, if there's no output when you run the program, that's
 * considered successfull.
 */


/**
 * Returns the number of copies of the gene 'gene' in the chromosom 'chr'.
 */ 
int geneCopiesInChr(ga::Chromosom *chr, ga::Gene *gene)
{
    int ret = 0;

    std::vector<ga::Gene *> genes = chr->getGenes();
    std::vector<ga::Gene *>::const_iterator it = genes.begin();

    while (it != genes.end()) {
        ga::Gene *g = (*it);

        if (*g == *gene)
            ++ret;

        ++it;
    }

    return ret;
}


/**
 * Returns true if the chromosom 'chr' contains uniques gene copies, otherwise
 * returns false.
 */
bool chrContainsUniqueCopies(ga::Chromosom *chr)
{
    bool ret = true;

    std::vector<ga::Gene *> genes = chr->getGenes();
    std::vector<ga::Gene *>::const_iterator it = genes.begin();

    while (it != genes.end()) {
        ga::Gene *g = (*it);

        int copyCount = geneCopiesInChr(chr, g);

        if (copyCount > 1) {
            ret = false;
            break;
        }

        ++it;
    }

    return ret;
}

/**
 * /!\ Since the unordered one point crossover could lead to different results,
 * (unique or several copies of each gene in the siblings), no assert is
 * present in the function body.
 */
void test_crossoverOnePointUnordered(ga::Chromosom *father,
                                     ga::Chromosom *mother)
{
    tsp::Path s1, s2;

    ga::Crossover *crossover = ga::Crossover::instance();

    crossover->cross(father, mother, &s1, &s2, ga::Crossover::OnePointCross);
}

void test_crossoverOnePointOrdered(ga::Chromosom *father,
                                   ga::Chromosom *mother)
{
    tsp::Path s1, s2;

    ga::Crossover *crossover = ga::Crossover::instance();

    crossover->cross(father, mother, &s1, &s2,
                     ga::Crossover::OnePointCrossOrdered);

    assert(chrContainsUniqueCopies(&s1));   
    assert(chrContainsUniqueCopies(&s2));   
}

/**
 * Instanciate some specialized chromosoms and genes, performs an crossover
 * pass and check that each individual contains one copy of each of their
 * genes.
 */
int main()
{
    utils::initRandEngine();

    // Create a path
    tsp::PathFactory factory;
    ga::Chromosom *father = factory.createRandom(4);
    ga::Chromosom *mother = factory.createRandom(4);
    
    test_crossoverOnePointUnordered(father, mother);
    test_crossoverOnePointOrdered(father, mother);

    return 0;
}
