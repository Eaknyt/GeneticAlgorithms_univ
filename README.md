Genetic Algorithms - C++ University Project
===========================================

Note
----

`Blackboard` is not provided with this source.

How to build
------------

    cd <ProjectSourceDir>
    make init

This will create a ``build`` folder above the source directory.

    make build_samples

And this will build the project samples.

Available targets
-----------------

    make init

Creates the build directory in the parent folder. Use this once you downloaded
the project or after you deleted the entire build.

    make clean

Removes C++ object files.

    make purge

Removes C++ object files and executable files. It doesn't remove the build
directory.

    make fake_install

Removes C++ objects files and its folder. Use it to mimick a clean build folder.

    make run_tests

Build the test files located in the ``tests`` folder and run them. If there's
no output, consider that all tests passed.

    make all

Build the lib, samples, tests and application.

See the `makefile` to see all targets.

Deal with Blackboard
--------------------

It may be a good idea to put the `Blackboard` archive in `<BuildDir>/bin`.

Here's is a snippet to run the sample provided with the software :

    # Build the sample
    cd <ProjectSourceDir>
    make sample_blackboard

    # Run Blackboard 
    java -jar ../build/bin/Blackboard.jar
    
    # /!\ Don't forget to bind Blackboard to the port 1500 using its GUI

    # Run the sample
    ../build/samples/sample_blackboard.exe 127.0.0.1 1500


Execute the main application
----------------------------

    # Build
    cd <ProjectSourceDir>
    make all

    # Run Blackboard 
    java -jar ../build/bin/Blackboard.jar
    
    # /!\ Don't forget to bind Blackboard to the port 1500 using its GUI

    # Run the application
    ../build/bin/main.exe <iterations> <individualCount> <geneCount>
