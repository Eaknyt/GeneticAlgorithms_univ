#include <assert.h>

#include "utils/Singleton.h"

/**
 * A dummy class to feed a Singleton for this sample
 */
class Foo
{
public:
    Foo() {}
    ~Foo() {}
};

int main()
{
    // Retrieve the singleton object for the first time
    Foo *f = utils::Singleton<Foo>::instance();

    // Verify that it exists
    assert(f);

    // Retrieve the same object !
    Foo *f2 = utils::Singleton<Foo>::instance();

    // Verify that both are the same
    assert(f == f2);

    return 0;
}
