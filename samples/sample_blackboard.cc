#include <string>

#include "graph/CClientTCP.h"

#include "tsp/Path.h"
#include "tsp/PathFactory.h"
#include "tsp/Town.h"

#include "utils/Point.h"
#include "utils/utilsfuncs.h"

// Because of the Blackboard's amazing coordinates system ;
//  Adjust these constants using your screen size.
const int SCREEN_SIZE_X = 1600 - 50;
const int SCREEN_SIZE_Y = 900 - 80;

const std::string BB_HOST = "127.0.0.1";
const int BB_PORT = 1500;

/**
 * This sample generates a random path and tell Blackboard to draw it.
 */
int main()
{
    // Init the bridge to BlackBoard
    CClientTCP client(BB_HOST, BB_PORT);

    // Init the random engine
    utils::initRandEngine();

    // Create a path
    tsp::PathFactory factory;
    factory.setArea(0, 0, SCREEN_SIZE_X, SCREEN_SIZE_Y);
    ga::Chromosom *path = factory.createRandom(16);

    std::vector<ga::Gene *> towns = path->getGenes();
    std::vector<ga::Gene *>::const_iterator it = towns.begin();

    // Draw the first town
    tsp::Town *firstTown = dynamic_cast<tsp::Town *>(*it);
    utils::Point firstCoords = firstTown->getCoords();
    
    utils::Point coords = firstTown->getCoords();
    client.drawCircle(coords.x(), coords.y(), 7, CColor::sBlue, true);

    ++it;

    // Draw paths and next towns
    while (it != towns.end()) {
        tsp::Town *t = dynamic_cast<tsp::Town *>(*it);
        coords = t->getCoords();
        
        client.drawCircle(coords.x(), coords.y(), 7, CColor::sBlue, true);

        tsp::Town *previous = dynamic_cast<tsp::Town *>(*(it - 1));
        utils::Point previousCoords = previous->getCoords();

        client.drawSegment(coords.x(), coords.y(),
                           previousCoords.x(), previousCoords.y(),
                           CColor::sWhite);
        ++it;
    }

    // Close the path
    tsp::Town *lastTown = dynamic_cast<tsp::Town *>(*(towns.end() - 1));
    utils::Point lastCoords = lastTown->getCoords();

    client.drawSegment(firstCoords.x(), firstCoords.y(),
                       lastCoords.x(), lastCoords.y(),
                       CColor::sWhite);

    // End the bridge
    client.disconnect();

    return 0;
}
