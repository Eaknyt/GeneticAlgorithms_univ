#include <iostream>

#include "ga/Mutator.h"

#include "tsp/Path.h"
#include "tsp/PathFactory.h"
#include "tsp/Town.h"

#include "utils/utilsfuncs.h"

int main()
{
    utils::initRandEngine();

    // Create a path
    tsp::PathFactory factory;
    ga::Chromosom *path = factory.createRandom(8);

    // Print before mutation
    std::cout << "Before mutation" << std::endl
              << "------------------------------"
              << std::endl;

    std::cout << (*path) << std::endl << std::endl;

    float fitnessBeforeMut = path->fitness();

    // Mutate
    ga::Mutator *mutator = ga::Mutator::instance();
    std::vector<ga::Chromosom *> chrs;
    chrs.push_back(path);

    mutator->mutate(chrs, 100);

    // Print after mutation
    std::cout << "After mutation" << std::endl
              << "------------------------------"
              << std::endl;

    std::cout << (*path) << std::endl;

    // Print fitnesses
    float fitnessAfterMut = path->fitness();

    std::cout << "Fitnesses" << std::endl
              << "------------------------------" << std::endl
              << "Before -> " << fitnessBeforeMut << std::endl
              << "After -> " << fitnessAfterMut
              << std::endl; 

    return 0;
}
